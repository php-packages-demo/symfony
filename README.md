<head> <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>

# [symfony](https://phppackages.org/s/symfony)/[symfony](https://phppackages.org/p/symfony/symfony)

[**symfony/symfony**](https://packagist.org/packages/symfony/symfony)
![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/symfony)


The Symfony PHP framework https://symfony.com

(Unofficial demo and howto)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-polyfill-php80+php-symfony-framework-bundle&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

2021 Increase of popularity may be linked with a new dependency of phpmyadmin.

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/console) symfony/console
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/finder) symfony/finder
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/process) symfony/process
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/event-dispatcher) symfony/event-dispatcher
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing) symfony/routing
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/filesystem) symfony/filesystem
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/config) symfony/config
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/dependency-injection) symfony/dependency-injection
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/cache) symfony/cache
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle) symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/expression-language) symfony/expression-language

[[_TOC_]]

# Official documentation
* Book: [*Symfony: The Fast Track*
  ](https://symfony.com/book)
* [Symfony Documentation](https://symfony.com/doc)
* [SymfonyCasts](https://symfonycasts.com/)
* [The SensioLabs Tech Blog: Insights from the creator of Symfony](https://medium.com/the-sensiolabs-tech-blog)

## Front-end Tools
* [*Front-end Tools: Handling CSS & JavaScript*
  ](https://symfony.com/doc/current/frontend.html)
  * Using PHP & Twig:
    * AssetMapper (Recommended)
    * Webpack Encore

### Stimulus & Symfony UX Components (UX initiative)
* [*StimulusBundle: Symfony integration with Stimulus*
  ](https://symfony.com/bundles/StimulusBundle/current/index.html)
* [*Symfony ux: JavaScript tools you can't live without*
  ](https://ux.symfony.com/)
---
* [*Symfony UX makes Symfony a Full Stack Framework (again)*
  ](https://symfony.fi/entry/symfony-ux-makes-symfony-a-full-stack-framework)
  2020-12 Jani Tarvainen
* [*New in Symfony: the UX initiative, a new JavaScript ecosystem for Symfony*
  ](https://symfony.com/blog/new-in-symfony-the-ux-initiative-a-new-javascript-ecosystem-for-symfony)
  2020-12  Titouan Galopin

### Using a Front-end Framework (React, Vue, Svelte, etc) with API Plateform

## News
* [*Symfony in 2019*](https://speakerdeck.com/fabpot/symfony-in-2019)
  2019-06 Fabien Potencier

## Releases
### 7.0
* [*Upgrade to Symfony v7*
  ](https://dev.to/hantsy/upgrade-to-symfony-v7-4dad)
  2024-02  Hantsy Bai (DEV)

### 6.3
* [*Features I Like in Symfony 6.3*
  ](https://martinmicka.medium.com/features-i-like-in-symfony-6-3-5a390fbd6f8a)
  2023-06 Martin Mička
* [*New in Symfony 6.3: OpenID Connect Token Handler*
  ](https://symfony.com/blog/new-in-symfony-6-3-openid-connect-token-handler)
  2023-04 Javier Eguiluz

### 6.0
* [*Preparing Your Apps and Bundles for Symfony 6*
  ](https://symfony.com/blog/preparing-your-apps-and-bundles-for-symfony-6)
  2021-09  Javier Eguiluz
* [*Symfony 6: PHP 8 Native Types & Why we Need YOU*
  ](https://wouterj.nl/2021/09/symfony-6-native-typing)
  2021-09 Wouter J
* [Living on the Edge](https://symfony.com/blog/category/living-on-the-edge/6.0)

### 5.4
* [*Symfony 5.4/6.0 curated new features*
  ](https://symfony.com/blog/symfony-5-4-6-0-curated-new-features)
  2021-12 Fabien Potencier
* [Living on the Edge](https://symfony.com/blog/category/living-on-the-edge/5.4)

### 5.3
* [Living on the Edge](https://symfony.com/blog/category/living-on-the-edge/5.3)
* [*Symfony 5.3 curated new features*
  ](https://symfony.com/blog/symfony-5-3-curated-new-features)
  2021-05 Fabien Potencier
* [*The Runtime Component*
  ](https://symfony.com/doc/current/components/runtime.html)
  * [*New in Symfony 5.3: Runtime Component*
    ](https://symfony.com/blog/new-in-symfony-5-3-runtime-component)
    2021-06 Javier Eguiluz
* (fr) [*Les bonnes raisons de mettre à jour vers Symfony 5.3 !*
  ](https://jolicode.com/blog/les-bonnes-raisons-de-mettre-a-jour-vers-symfony-5-3) 2021 joliCode

### 5.2
* [Living on the Edge](https://symfony.com/blog/category/living-on-the-edge/5.2)
* [*5 New Combos opened by Symfony 5.2 and PHP 8.0*
  ](https://tomasvotruba.com/blog/2020/12/21/5-new-combos-opened-by-symfony-52-and-php-80/)
  2020-12 tomasvotruba

### 5.1
* [Living on the Edge](https://symfony.com/blog/category/living-on-the-edge/5.1)

### 5.0
* [Living on the Edge](https://symfony.com/blog/category/living-on-the-edge/5.0)

### 4.3
* [*Symfony 4.3 curated new features*
  ](https://symfony.com/blog/symfony-4-3-curated-new-features).
  2019 Fabien Potencier
* [Living on the Edge](https://symfony.com/blog/category/living-on-the-edge/4.3)
* [*Symfony 4.2 curated new features*
  ](https://symfony.com/blog/symfony-4-2-curated-new-features)
  2018-11 Fabien Potencier

## Deprecated...
### WebserverBundle
* "deprecated WebserverBundle" in [*A Week of Symfony #654 (8-14 July 2019)*
  ](https://symfony.com/blog/a-week-of-symfony-654-8-14-july-2019)
  2019-07 Javier Eguiluz

### Swift Mailer (and his bundle)
* As of November 2021, Swiftmailer will not be maintained anymore. Use [Symfony Mailer](https://symfony.com/doc/current/mailer.html) instead. Read more on [Symfony's blog](https://symfony.com/doc/current/mailer.html).
  * A bundle may no more be needed.
  * [*Sending Emails with Mailer*
    ](https://symfony.com/doc/current/mailer.html)

### Templating component integration
* [*New in Symfony 4.3: Deprecated the Templating component integration*
  ](https://symfony.com/blog/new-in-symfony-4-3-deprecated-the-templating-component-integration)
  2019-04 Javier Eguiluz

### DoctrineCacheBundle
* [*[RFC] Deprecation of DoctrineCacheBundle #156*
  ](https://github.com/doctrine/DoctrineCacheBundle/issues/156)
  2019-04 alcaeus
  * https://github.com/doctrine/DoctrineCacheBundle

### Symfony 4.2
* [*New in Symfony 4.2: Important deprecations*
  ](https://symfony.com/blog/new-in-symfony-4-2-important-deprecations)
  2018-10 Javier Eguiluz

### Searching for deprecated things...
* [New in Symfony Deprecated](https://www.google.com/search?q=New+in+Symfony+Deprecated)
* [symfony depercate deprecated deprecation site:symfony.com/blog
  ](https://google.com/search?q=symfony+depercate+deprecated+deprecation+site%3Asymfony.com%2Fblog)

# Searching things
## Logger
* Symfony comes with a minimalist PSR-3 logger: Logger. It is in the component http-kernel.

## DATABASE_URL
* [symfony database_url](https://google.com/search?q=symfony+database_url)

# Ranking Symfony packages
* https://phppackages.org/s/symfony
* https://libraries.io/packagist/symfony%2Fsymfony
* [*10 bundles you should know if you are going to work with Symfony*
  ](https://medium.com/@ger86/10-packages-you-should-know-if-you-are-going-to-work-with-symfony-497b116be428)
  2019-08 Gerardo Fernández
* [*Is Zend Dead? Is Laravel Losing Breath? Trends of PHP Frameworks in Numbers*
  ](https://www.tomasvotruba.cz/blog/2019/04/11/trends-of-php-frameworks-in-numbers/)
  2019-04 Tomas Votruba
  * [*PHP Framework Trends*](https://www.tomasvotruba.cz/php-framework-trends/)
  * [*Package Monthly Downloads by Version*
    ](https://www.tomasvotruba.cz/package-downloads-by-version/)
  * [*Symfony 4.2 is used Twice More than Symfony 3.4*
    ](https://www.tomasvotruba.cz/blog/2019/04/18/symfony-4-2-is-used-2-times-more-than-symfony-3-4/)

# Made with Symfony
* [*Similarities Between Spryker and Symfony*
  ](https://medium.com/@shivkumar_57196/similarities-between-spryker-and-symfony-d65ea5e32698)
  2023-10 Shiv Kumar (Medium)
* [*Exploring the 12 eCommerce Platforms of Symfony*
  ](https://medium.com/@mobileatom/exploring-the-12-ecommerce-platforms-of-symfony-219122a77a0a)
  2022-03 Reuben Walker
* [*Exploring the 17 Content Management Systems of Symfony*
  ](https://www.symfonystation.com/content-management-systems-symfony)
  2022-01 Reuben Walker
  * "Some of them have the capability to incorporate Symfony Bundles. For example, it's likely with eZ Platform/Ibexa, Sulu, Bolt, Contao, Fork, and Kunstmaan. Most are customizable."

# FontAwesome logo
<i class="fas fa-puzzle-piece" aria-hidden="true"></i>
<i class="fab fa-symfony"></i>
* [*FontAwesome adds Symfony to their icon collection*
  ](https://symfony.com/blog/fontawesome-adds-symfony-to-their-icon-collection) 2019
* https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/#font-awesome

# Unofficial documentation
## (Weekly) News
* [Symfony Station Communiques: Weekly reviews of Symfony and PHP news
  ](https://symfonystation.godaddysites.com)

## Tutorials
* See also "Deployment"
---
* [*Symfony Tutorial*
  ](https://tutorialspoint.com/symfony)
  (Tutorials Point)
---
* [*Hello from Symfony*
  ](https://dev.to/abdounikarim/hello-from-symfony-20me)
  2024-12 Abdouni Abdelkarim (DEV)
* [*Creating a Symfony project from zero to success in 52 weeks (week 1)*
  ](https://darielvicedo.medium.com/creating-a-symfony-project-from-zero-to-success-in-52-weeks-week-1-1aa158674d95)
  2022-07 Dariel Vicedo
* [*Building a Shopping Cart with Symfony*
  ](https://dev.to/qferrer/introduction-building-a-shopping-cart-with-symfony-f7h)
  (11 Part Series)
  2020-12 Quentin
* [*A simple recipe for framework decoupling*
  ](https://matthiasnoback.nl/2020/09/simple-recipe-for-framework-decoupling/)
  2020-06 Matthias Noback
* [*Symfony Tutorial: Building a Blog (Part 1)*
  ](https://auth0.com/blog/symfony-tutorial-building-a-blog-part-1/)
  2018-02 Greg Holmes
* [*Symfony Tutorial: Building a Blog (Part 2)*
  ](https://auth0.com/blog/creating-symfony-blog-part-2/)
  2018-02 Greg Holmes
* [*Symfony Tutorial: Building a Blog (Part 3)*
  ](https://auth0.com/blog/symfony-tutorial-building-a-blog-part-3/)
  2018-03 Greg Holmes
* [*Symfony2: How to create framework independent controllers?*
  ](https://matthiasnoback.nl/2014/06/how-to-create-framework-independent-controllers/)
  2014-06 Matthias Noback
  * *Part I: Don't use the standard controller*
  * [*Symfony2: Framework independent controllers part 2: Don't use annotations*
    ](https://matthiasnoback.nl/2014/06/don-t-use-annotations-in-your-controllers/)
  * I consider this article deprecated. Controllers don't need decoupling, domain logic does. See [*A simple recipe for framework decoupling*](https://matthiasnoback.nl/2020/09/simple-recipe-for-framework-decoupling/)


### French section
* (fr) [*L’enseignement de Symfony 5 à des étudiants d’une école Tech*
  ](https://medium.com/@kevin.nadin/lenseignement-de-symfony-5-%C3%A0-des-%C3%A9tudiants-d-une-%C3%A9cole-tech-2fe31ed70bef)
  2020-01 Kevin Nadin
* (fr) [*workeet.app Part 1 : Symfony est ton ami*
  ](https://medium.com/@gu.bordes/workeet-app-part-1-symfony-est-ton-ami-a77d28d1bba)
  2018-09 gbtux

#### (fr) CMS en Symfony
* (fr) [*CMS en Symfony : quelles fonctionnalités ?*
  ](https://dev.to/gbtux/cms-en-symfony-quelles-fonctionnalites--35ln)
  2022-02 Guillaume
* (fr) [*CMS en Symfony : le routing*
  ](https://dev.to/gbtux/cms-en-symfony-le-routing-2ngo)
  2022-02 Guillaume

## Advice
### French section
* (fr) [*Mon top 5 des PIRES erreurs sous Symfony*
  ](https://blog.eleven-labs.com/fr/top-5-des-pires-erreurs-sous-symfony/)
  2022-08 Marianne Joseph-Géhannin

## Advocating for...
* [*8 Reasons To Create Business Applications & Digital Marketplaces with Symfony PHP Framework*
  ](https://medium.com/@etondigital/8-reasons-to-create-business-applications-digital-marketplaces-with-symfony-php-framework-97320179f70f)
  2019-11 Eton Digital Blog

## Architecture
* [*Unlock the Secrets of Symfony’s Kernel Events: What Every Developer Must Know!*
  ](https://medium.com/@skowron.dev/unlock-the-secrets-of-symfonys-kernel-events-what-every-developer-must-know-7ec39f3fc003)
  2024-05 Jakub Skowron (skowron.dev, Medium)
  * But Symfony is itself a call-back for the event-driven webserver!
* [*Modernizing PHP apps using DDD with Symfony*
  ](https://medium.com/@razvandubau/modernizing-php-app-using-ddd-with-symfony-60a58ed562bf)
  2024-03 Razvan Dubau
* [*Enhancing Code Decoupling in Symfony with Immutable Data Transfer Objects (DTOs)*
  ](https://itnext.io/enhancing-code-decoupling-in-symfony-with-immutable-data-transfer-objects-dtos-7b4d6d32005f)
  2024-02 Nikolay Nikolov (Medium)
* [*Implementing the Factory Method Design Pattern in Symfony*
  ](https://medium.com/@rcsofttech85/implementing-the-factory-method-design-pattern-in-symfony-b9547a002737)
  2023-10 rcsofttech85 (Medium)
* (fr) [*Symfony-Less, ou comment rendre son code indépendant du framework PHP Symfony: introduction*
  ](https://medium.com/@etiennekoumgang/symfony-less-ou-comment-rendre-son-code-ind%C3%A9pendant-du-framework-php-symfony-introduction-1badbc449b9e)
  2022-05 Etienne koumgang
  * Maybe first article of a series of articles.
### Hexagonal architecture
* [*How to apply Hexagonal/Clean/Onion architecture with only two changes to your symfony project*
  ](https://saeidme.medium.com/how-to-apply-hexagonal-clean-onion-architecture-with-only-two-changes-to-your-symfony-project-2c5fe16d5a4f)
  2022-08 Saeid Raei
* (fr) [*L’archi Hexa est-ce archi bien❓*
  ](https://smaine-milianni.medium.com/larchi-hexa-est-ce-archi-bien-fd0449b32e33)
  2021-11 Smaine Milianni
  * (en) soon
* [*Symfony and Hexagonal Architecture*
  ](https://minompi.medium.com/symfony-and-hexagonal-architecture-b3c4704e94de)
  2020-12 AlessandroMinoccheri
* [*Applying Hexagonal Architecture to a Symfony project*
  ](https://apiumhub.com/tech-blog-barcelona/applying-hexagonal-architecture-symfony-project/)
  2018 Javier Gomez

## Checkup and CI (and static analysis)
* [*Creating custom PHPStan rules for your Symfony project*
  ](https://www.strangebuzz.com/en/blog/creating-custom-phpstan-rules-for-your-symfony-project)
  2021-10 C0il
* [*How to ensure PHP code quality? Static analysis using PHPStan with Symfony*
  ](https://medium.com/accesto/how-to-ensure-php-code-quality-static-analysis-using-phpstan-with-symfony-accesto-blog-39fb57545e44)
  2021-07 Michał Romańczuk
* [*Seven Commands to Bulletproof Your Symfony Application CI Build*
  ](https://medium.com/@kamkok512/seven-commands-to-bulletproof-your-symfony-application-ci-build-94c1a3bf7176)
  2020-01 Kamil Kokot
* [*How to set up a PHP Symfony application test pipeline on Gitlab CI*
  ](https://medium.com/@elvisciotti/how-to-set-up-a-php-symfony-application-test-pipeline-on-gitlab-ci-8a071df56b)
  2019-11 E Ciotti
* [*How to configure PHPStan for Symfony applications*
  ](https://blog.martinhujer.cz/how-to-configure-phpstan-for-symfony-applications/)
  2019-10 Martin Hujer

## Deployment
* [*How to Install Symfony 5 on Ubuntu 18.04 | 16.04 with Nginx*
  ](https://websiteforstudents.com/how-to-install-symfony-5-on-ubuntu-18-04-16-04-with-nginx/)
  2020-01
* [*How to Install Symfony 5 Framework on Ubuntu 18.04 | 16.04 with Apache2*
  ](https://websiteforstudents.com/how-to-install-symfony-5-framework-on-ubuntu-18-04-16-04-with-apache2/)
  2020-01
* [*How to Deploy Symfony with Nginx on Ubuntu 18.04*
  ](https://clouding.io/kb/en/how-to-deploy-symfony-with-nginx-on-ubuntu-18-04/)
  2019-10
* [*How to Deploy a Symfony 4 Application to Production with LEMP on Ubuntu 18.04*
  ](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-4-application-to-production-with-lemp-on-ubuntu-18-04)
  2018 [Oluyemi Olususi](https://www.digitalocean.com/community/users/yemiwebby)

## Hosting
* [*How to run Symfony on Google Cloud Run with the demo app [Step-by-Step Guide]*
  ](https://geshan.com.np/blog/2019/11/how-to-run-symfony-on-google-cloud-run-with-the-demo-app-step-by-step-guide/)
  2019-11 Geshan Manandhar

## Logging
* [*How to log your life easier in Symfony?*
  ](https://dev.to/medunes/how-to-log-your-life-easier-in-symfony-5np)
  2022-03 medunes

## Memory Leaks
* [*You may have memory leaking from PHP 7 and Symfony tests*
  ](https://jolicode.com/blog/you-may-have-memory-leaking-from-php-7-and-symfony-tests)
  2019-10 Damien Alexandre

## PHP and Symfony (PHP 8.1, 8)
* [*Using Enum in Symfony*
  ](https://itnext.io/using-enum-in-symfony-8f32d9cfaa0f?gi=fa0224a4257a)
  2022-02 Hantsy
* (fr) [*Les attributs PHP 8 dans Symfony*
  ](https://www.elao.com/blog/dev/les-attributs-php-8-dans-symfony)
  Utilisation des attributs PHP 8 à la place des annotations.
  2021-07 Maxime Colin

## ReactPHP and Symfony
* [php-packages-demo/apisearch-io/symfony-async-http-kernel
  ](https://gitlab.com/php-packages-demo/apisearch-io-symfony-async-http-kernel)
* [*Super Speed Symfony - ReactPHP*](https://gnugat.github.io/2016/04/13/super-speed-sf-react-php.html).
  2016 Loïc Faugeron

# Advice
## Efficiency
* [*Symfony Caching and Performance Boosters: Unlocking the Power of Efficiency*
  ](https://david-garcia.medium.com/symfony-caching-and-performance-boosters-unlocking-the-power-of-efficiency-4424d7d2c609)
  2023-06 David Garcia (Medium)
* [*Tune-in Symfony Performance with These Optimization Tips*
  ](https://innmind.com/articles/2118)
  2019-10 Akashdeep Sharma

### Bare minimum ([*SymfonyLive London 2018: summary*](https://medium.com/@coopTilleuls/symfonylive-london-2018-summary-b229bad5e3d))
> For a more efficient application, limit your code to the bare essentials and
> remove everything you do not need. Maybe you only need the
> [router](https://symfony.com/components/Routing)? Or only
> [cache](https://symfony.com/components/Cache)? The minimum could then be the
> [HttpFoundation](https://symfony.com/components/HttpFoundation) component with
> the Cache component. A little more complexity? Then add the
> [MicroKernelTrait](https://symfony.com/doc/current/configuration/micro_kernel_trait.html).

# Open-Source cross-pollination
* [*Open-Source cross-pollination*
  ](https://symfony.com/blog/open-source-cross-pollination)
  2008-09 Fabien Potencier
  * Prado (I18N)
  * Ruby on Rails (helpers (of console components?))
  * Form from
    * Django
    * Formencode
  * Dependency Injection from
    * Java Spring framework
  * Symfony web debug toolbar ported to
    * Django

## Live Components inspired by [Livewire](https://laravel-livewire.com/) and [Phoenix LiveView](https://hexdocs.pm/phoenix_live_view/Phoenix.LiveView.html)
* [*Live components*](https://github.com/symfony/ux-live-component)

## Java Spring framework inspiration in Symfony
* https://spring.io/projects/spring-framework
* [symfony Java Spring framework](https://google.com/search?q=symfony+Java+Spring+framework)

### Official recognition
* https://github.com/symfony/security-core
* https://github.com/symfony/security-http

## symfony/css-selector
* From python cssselect

# Alternative to Symfony
## JavaScript
### nest
* [nestjs.com](https://nestjs.com)
* npm: [nestjscore](https://www.npmjs.com/~nestjscore)
* Inspired by [*Angular (web framework)*](https://en.wikipedia.org/wiki/Angular_(web_framework))
* [*Symfony vs. Nest.js: The Ultimate Showdown for Modern Web Development*
  ](https://vulke.medium.com/symfony-vs-nest-js-the-ultimate-showdown-for-modern-web-development-321d01931387)
  2024-07 Ivan Vulovic (Medium)

# Packages
## Contracts

## Composer plugin
* 
  symfony/flex

## Polyfill
* 
  symfony/polyfill-mbstring
  * Required by
    * symfony/framework-bundle

## Related extensions
* ext-apcu
  * Suggested by
    * symfony/framework-bundle
* ext-xml
  * Required by
    * symfony/framework-bundle

## PHP version polyfill
* 
  symfony/polyfill-php70

## Components
![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

2021 Increase of popularity may be linked with a new dependency of phpmyadmin.

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/console) symfony/console
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/finder) symfony/finder
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/process) symfony/process
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/event-dispatcher) symfony/event-dispatcher
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing) symfony/routing
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/filesystem) symfony/filesystem
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/config) symfony/config
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/dependency-injection) symfony/dependency-injection
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/cache) symfony/cache
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/expression-language) symfony/expression-language

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-event-dispatcher+php-symfony-routing+php-symfony-http-foundation+php-symfony-http-kernel&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=2014-01-01&to_date=&hlght_date=&date_fmt=%25Y)

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/event-dispatcher) symfony/event-dispatcher
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-foundation) symfony/http-foundation
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing) symfony/routing
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-kernel) symfony/http-kernel

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-http-foundation+php-symfony-mime+php-symfony-http-kernel+php-symfony-error-handler+php-symfony-property-access+php-symfony-css-selector+php-symfony-property-info+php-symfony-intl+php-symfony-options-resolver&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-foundation) symfony/http-foundation
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/mime) symfony/mime
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-kernel) symfony/http-kernel
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/error-handler) symfony/error-handler
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/property-access) symfony/property-access
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/css-selector) symfony/css-selector
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/property-info) symfony/property-info
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/intl) symfony/intl
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/options-resolver) symfony/options-resolver

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-options-resolver+php-symfony-dom-crawler+php-symfony-browser-kit+php-symfony-ldap+php-symfony-lock+php-symfony-asset+php-symfony-form+php-symfony-security-core+php-symfony-security-csrf&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/options-resolver) symfony/options-resolver
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/dom-crawler) symfony/dom-crawler
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/browser-kit) symfony/browser-kit
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/ldap) symfony/ldap
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/lock) symfony/lock
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/asset) symfony/asset
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/form) symfony/form
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/security-core) symfony/security-core
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/security-csrf) symfony/security-csrf

![Debian popularity percent](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=0&show_vote=1&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=2014-01-01&to_date=&hlght_date=&date_fmt=%25Y)

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/filesystem) symfony/filesystem
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/console) symfony/console
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/process) symfony/process
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/finder) symfony/finder
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/expression-language) symfony/expression-language
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/cache) symfony/cache
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/config) symfony/config
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/dependency-injection) symfony/dependency-injection
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/event-dispatcher) symfony/event-dispatcher


* [*Ultimate Guide to Symfony Components (2021)*
  ](https://alex-daubois.medium.com/ultimate-guide-to-symfony-components-2021-7ac9908f8aa2)
  2021-09 Alexandre Daubois
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/console)
  symfony/console
  * Suggested by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle)
      doctrine/doctrine-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/process)
  symfony/process
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/distribution-bundle)
      sensio/distribution-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/finder)
  symfony/finder
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/event-dispatcher)
  symfony/event-dispatcher
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/var-dumper)
  symfony/var-dumper
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-foundation)
  symfony/http-foundation
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing)
  symfony/routing
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-kernel)
  symfony/http-kernel
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/distribution-bundle)
      sensio/distribution-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/translation)
  symfony/translation
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/yaml)
  symfony/yaml
  * Suggested by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/css-selector)
  symfony/css-selector
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/filesystem)
  symfony/filesystem
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/distribution-bundle)
      sensio/distribution-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/config)
  symfony/config
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle)
      doctrine/doctrine-bundle
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/distribution-bundle)
      sensio/distribution-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/dependency-injection)
  symfony/dependency-injection
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle)
      doctrine/doctrine-bundle
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/distribution-bundle)
      sensio/distribution-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/stopwatch)
  symfony/stopwatch
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/validator)
  symfony/validator
  * Suggested by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/asset)
  symfony/asset
  [doc](https://symfony.com/doc/current/components/asset.html)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/debug)
  symfony/debug
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
      symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/templating)
  symfony/templating
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/class-loader)
  symfony/class-loader
  * Required by
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/distribution-bundle)
      sensio/distribution-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/thanks)
  symfony/thanks
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/asset-mapper)
  symfony/asset-mapper
  [doc](https://symfony.com/doc/current/frontend/asset_mapper.html)
  * Requires
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/asset)
      symfony/asset
    * twig

### Third-party components
*
  drift/http-kernel (62938)

## Bridges
* 
  symfony/doctrine-bridge

## Packs
* [All Symfony Components](https://symfony.com/components)
---
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/apache-pack)
  symfony/apache-pack
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/orm-pack)
  symfony/orm-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/orm)
    doctrine/orm
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle)
    doctrine/doctrine-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-migrations-bundle)
    doctrine/doctrine-migrations-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/serializer-pack)
  symfony/serializer-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/annotations)
    doctrine/annotations
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/phpdocumentor/reflection-docblock) 
    phpdocumentor/reflection-docblock
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/phpstan/phpdoc-parser) phpstan/phpdoc-parser
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/property-access)
    symfony/property-access
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/property-info)
    symfony/property-info
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/serializer)
    symfony/serializer
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/profiler-pack)
  symfony/profiler-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/stopwatch)
    symfony/stopwatch
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/web-profiler-bundle)
    symfony/web-profiler-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/test-pack)
  symfony/test-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/phpunit/phpunit)
    phpunit/phpunit
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/browser-kit)
    symfony/browser-kit
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/css-selector)
    symfony/css-selector
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/phpunit-bridge)
    symfony/phpunit-bridge
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/twig-pack)
  symfony/twig-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/twig)
    twig/twig
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/twig-bundle)
    symfony/twig-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/extra-bundle)
    twig/extra-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/debug-pack)
  symfony/debug-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/debug-bundle)
    symfony/debug-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/monolog-bundle)
    symfony/monolog-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/profiler-pack)
    symfony/profiler-pack
* ![Packagist Downloads](https://img.shields.io/packagist/dm/api-platform/api-pack)
  api-platform/api-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/api-platform/core)
    api-platform/core
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/nelmio/cors-bundle)
    nelmio/cors-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/asset)
    symfony/asset
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/expression-language)
    symfony/expression-language
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/orm-pack)
    symfony/orm-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/security-bundle)
    symfony/security-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/serializer-pack)
    symfony/serializer-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/twig-bundle)
    symfony/twig-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/validator)
    symfony/validator
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/amqp-pack)
  symfony/amqp-pack
  * ext-amqp
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/messenger)
    symfony/messenger
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/serializer-pack)
    symfony/serializer-pack
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/webpack-encore-pack)
  symfony/webpack-encore-pack
  * Deprecated?, use symfony/webpack-encore-bundle or (recommended) symfony/asset-mapper

## Bundles
* [*The Bundle System*
  ](https://symfony.com/doc/current/bundles.html)
* [*Symfony Bundles Documentation*
  ](https://symfony.com/bundles)
* [Symfony bundles at GitHub](https://github.com/search?q=topic%3Asymfony-bundle&type=Repositories)
* [Symfony bundles at Debian](https://packages.debian.org/en/-bundle)
  * https://tracker.debian.org/pkg/symfony
    * php-symfony-debug-bundle
    * php-symfony-framework-bundle
    * php-symfony-security-bundle
    * php-symfony-twig-bundle
    * php-symfony-web-profiler-bundle
  * https://tracker.debian.org/pkg/php-doctrine-bundle
  * https://tracker.debian.org/pkg/php-symfony-mercure-bundle
  * https://tracker.debian.org/pkg/php-twig
    * php-twig-extra-bundle
---
![Popularity of Symfony bundles at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-framework-bundle+php-symfony-twig-bundle+php-symfony-security-bundle+php-symfony-web-profiler-bundle+php-symfony-debug-bundle+php-doctrine-bundle+php-twig-extra-bundle+php-symfony-mercure-bundle&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle) symfony/framework-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/twig-bundle) symfony/twig-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/security-bundle) symfony/security-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/web-profiler-bundle) symfony/web-profiler-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/debug-bundle) symfony/debug-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle) doctrine/doctrine-bundle (no more in recent Debian)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/extra-bundle) twig/extra-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/mercure-bundle) symfony/mercure-bundle

### Bundles with M/month popularity
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/framework-bundle)
  symfony/framework-bundle
  * Suggests
    * ext-apcu: For best performance of the system caches
    * symfony/console: For using the console commands
    * symfony/form:* For using forms
    * symfony/serializer*: For using the serializer service
    * symfony/validator: For using validation
    * symfony/yaml: For using the debug:config and lint:yaml commands
    * symfony/property-info*: For using the property_info service
    * symfony/web-link*: For using web links, features such as preloading, prefetching or prerendering
  * Required by all Symfony bundles (sure ?)
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/monolog-bundle)
  symfony/monolog-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/twig-bundle)
  symfony/twig-bundle
  * symfony/twig-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/twig)
    twig/twig
* ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle)
  doctrine/doctrine-bundle
  * symfony/orm-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/orm)
    doctrine/orm
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/security-bundle)
  symfony/security-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-migrations-bundle)
  doctrine/doctrine-migrations-bundle
* symfony/orm-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/orm)
    doctrine/orm
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-bundle)
    doctrine/doctrine-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-migrations-bundle)
    doctrine/doctrine-migrations-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/web-profiler-bundle)
  symfony/web-profiler-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/debug-bundle)
  symfony/debug-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/maker-bundle)
  symfony/maker-bundle
  (~~sensio/generator-bundle~~ Abandoned!)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/extra-bundle)
  twig/extra-bundle
* symfony/twig-pack
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/twig)
    twig/twig
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/twig-bundle)
    symfony/twig-bundle
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/twig/extra-bundle)
    twig/extra-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/nelmio/cors-bundle)
  nelmio/cors-bundle
* api-platform/api-pack
  * ...
  * ![Packagist Downloads](https://img.shields.io/packagist/dm/nelmio/cors-bundle)
    nelmio/cors-bundle

### Bundles with 900k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-fixtures-bundle)
  doctrine/doctrine-fixtures-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/framework-extra-bundle)
  ~~sensio/framework-extra-bundle~~
   Abandoned!

### Bundles with 800k/month popularity

### Bundles with 700k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/stof/doctrine-extensions-bundle)
  stof/doctrine-extensions-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/webpack-encore-bundle)
  symfony/webpack-encore-bundle
  * See also
    * ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/asset-mapper)
      symfony/asset-mapper (Recommended)

### Bundles with 600k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/lexik/jwt-authentication-bundle)
  lexik/jwt-authentication-bundle
  * Can be used with or without api-platform.
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/serializer-bundle)
  jms/serializer-bundle
  * Compare with symfony/serializer-pack

### Bundles with 500k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/nelmio/api-doc-bundle)
  nelmio/api-doc-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/friendsofsymfony/rest-bundle)
  friendsofsymfony/rest-bundle
  * Compare with api-platform/api-pack

### Bundles with 400k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/knplabs/knp-menu-bundle)
  knplabs/knp-menu-bundle

### Bundles with 300k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/ux-twig-component)
  symfony/ux-twig-component
  [Symfony Doc](https://symfony.com/bundles/ux-twig-component/current/index.html)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/knplabs/knp-paginator-bundle)
  knplabs/knp-paginator-bundle
  * Compare with easyadmin-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/liip/imagine-bundle)
  liip/imagine-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/stimulus-bundle)
  symfony/stimulus-bundle
  [Symfony Doc](https://symfony.com/bundles/StimulusBundle/current/index.html)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/swiftmailer-bundle)
  ~~symfony/swiftmailer-bundle~~
  * This package is abandoned and no longer maintained. The author suggests using the ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/mailer) symfony/mailer package instead. 

### Bundles with 200k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/snc/redis-bundle)
  snc/redis-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/knplabs/knp-gaufrette-bundle)
  knplabs/knp-gaufrette-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/block-bundle)
  sonata-project/block-bundle

### Bundles with 100k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/easycorp/easyadmin-bundle)
  easycorp/easyadmin-bundle
  (javiereguiluz/easyadmin-bundle)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-cache-bundle)
  doctrine/doctrine-cache-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/oneup/flysystem-bundle)
  oneup/flysystem-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/hautelook/alice-bundle)
  [hautelook/alice-bundle](https://packagist.org/packages/hautelook/alice-bundle)
  * fixtures
* ![Packagist Downloads](https://img.shields.io/packagist/dm/friendsofsymfony/user-bundle)
  ~~friendsofsymfony/user-bundle~~
  * New projects should not use this bundle.
* ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/mongodb-odm-bundle)
  doctrine/mongodb-odm-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/hwi/oauth-bundle)
  hwi/oauth-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/ux-live-component)
  symfony/ux-live-component
  [Symfony Doc](https://symfony.com/bundles/ux-live-component/current/index.html)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/php-amqplib/rabbitmq-bundle)
  php-amqplib/rabbitmq-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/willdurand/hateoas-bundle)
  willdurand/hateoas-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/willdurand/js-translation-bundle)
  willdurand/js-translation-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/exercise/htmlpurifier-bundle)
  exercise/htmlpurifier-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/nelmio/security-bundle)
  nelmio/security-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/routing-bundle)
  symfony-cmf/routing-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/admin-bundle)
  sonata-project/admin-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/friendsofsymfony/elastica-bundle)
  friendsofsymfony/elastica-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/winzou/state-machine-bundle)
  winzou/state-machine-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/doctrine-orm-admin-bundle)
  sonata-project/doctrine-orm-admin-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/payum/payum-bundle)
  payum/payum-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sylius/resource-bundle)
  sylius/resource-bundle
  * (fr) [*Sylius Resource Bundle - Gérez vos entités plus simplement*
    ](https://afsy.fr/avent/2019/23-sylius-resource-bundle-gerez-vos-entites)

### Bundles with 90k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/presta/sitemap-bundle)
  presta/sitemap-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/ekino/newrelic-bundle)
  ekino/newrelic-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/assetic-bundle)
  ~~symfony/assetic-bundle~~
  * This package is abandoned and no longer maintained. The author suggests using the symfony/webpack-encore-pack package instead.
  * But now symfony/asset-mapper is recommended.
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/distribution-bundle)
  ~~sensio/distribution-bundle~~
  * This package is abandoned and no longer maintained. No replacement package was suggested.

### Bundles with 80k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/eightpoints/guzzle-bundle)
  eightpoints/guzzle-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/knplabs/knp-time-bundle)
  knplabs/knp-time-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/translation-bundle)
  jms/translation-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/excelwebzone/recaptcha-bundle)
  excelwebzone/recaptcha-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/liip/functional-test-bundle)
  liip/functional-test-bundle

### Bundles with 70k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/oneup/uploader-bundle)
  oneup/uploader-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/willdurand/geocoder-bundle)
  willdurand/geocoder-bundle

### Bundles with 60k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/a2lix/translation-form-bundle)
  a2lix/translation-form-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/fresh/doctrine-enum-bundle)
  fresh/doctrine-enum-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/helios-ag/fm-elfinder-bundle)
  helios-ag/fm-elfinder-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/laupifrpar/pusher-bundle)
  laupifrpar/pusher-bundle

### Bundles with 50k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/liip/monitor-bundle)
  liip/monitor-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/datagrid-bundle)
  sonata-project/datagrid-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/intl-bundle)
  sonata-project/intl-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/media-bundle)
  sonata-project/media-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/gregwar/captcha-bundle)
  gregwar/captcha-bundle

### Bundles with 40k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/craue/formflow-bundle)
  craue/formflow-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/web-server-bundle)
  ~~symfony/web-server-bundle~~
  * This package is abandoned and no longer maintained. No replacement package was suggested.

### Bundles with 30k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/leezy/pheanstalk-bundle)
  leezy/pheanstalk-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/user-bundle)
  sonata-project/user-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/core-bundle)
  sonata-project/core-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/aop-bundle)
  jms/aop-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/misd/phone-number-bundle)
  misd/phone-number-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/i18n-routing-bundle)
  jms/i18n-routing-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/classification-bundle)
  sonata-project/classification-bundle

### Bundles with 20k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/nelmio/solarium-bundle)
  nelmio/solarium-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/lexik/translation-bundle)
  lexik/translation-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/knplabs/knp-markdown-bundle)
  knplabs/knp-markdown-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/stfalcon/tinymce-bundle)
  stfalcon/tinymce-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/phpcr-bundle)
  doctrine/phpcr-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/contao/core-bundle)
  contao/core-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/suncat/mobile-detect-bundle)
  suncat/mobile-detect-bundle

### Bundles with 10k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/formatter-bundle)
  sonata-project/formatter-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/lexik/form-filter-bundle)
  lexik/form-filter-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/gos/web-socket-bundle)
  gos/web-socket-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/scheb/two-factor-bundle)
  scheb/two-factor-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/liuggio/excelbundle)
  liuggio/excelbundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/easy-extends-bundle)
  sonata-project/easy-extends-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/seo-bundle)
  sonata-project/seo-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/liip/theme-bundle)
  liip/theme-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/security-extra-bundle)
  jms/security-extra-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/apy/breadcrumbtrail-bundle)
  apy/breadcrumbtrail-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/egeloen/ckeditor-bundle)
  egeloen/ckeditor-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/craue/config-bundle)
  craue/config-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/tbbc/money-bundle)
  tbbc/money-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/simplethings/entity-audit-bundle)
  simplethings/entity-audit-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sulu/sulu)
  sulu/sulu
* ![Packagist Downloads](https://img.shields.io/packagist/dm/ddeboer/vatin-bundle)
  ddeboer/vatin-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/job-queue-bundle)
  jms/job-queue-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/lexik/maintenance-bundle)
  lexik/maintenance-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/nelmio/js-logger-bundle)
  nelmio/js-logger-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/debril/rss-atom-bundle)
  debril/rss-atom-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/pugx/autocompleter-bundle)
  pugx/autocompleter-bundle

### Bundles with 1k/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/apy/datagrid-bundle)
  apy/datagrid-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/bolt/core)
  bolt/core
* ![Packagist Downloads](https://img.shields.io/packagist/dm/friendsofsymfony/comment-bundle)
  friendsofsymfony/comment-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/genemu/form-bundle)
  genemu/form-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/braincrafted/bootstrap-bundle)
  braincrafted/bootstrap-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/kunstmaan/bundles-cms)
  kunstmaan/bundles-cms
* ![Packagist Downloads](https://img.shields.io/packagist/dm/richsage/rms-push-notifications-bundle)
  richsage/rms-push-notifications-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/willdurand/faker-bundle)
  willdurand/faker-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/friendsofsymfony/message-bundle)
  friendsofsymfony/message-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/payment-core-bundle)
  jms/payment-core-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/egeloen/google-map-bundle)
  egeloen/google-map-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/page-bundle)
  sonata-project/page-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/besimple/i18n-routing-bundle)
  besimple/i18n-routing-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/elao/web-profiler-extra-bundle)
  elao/web-profiler-extra-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/whiteoctober/breadcrumbs-bundle)
  whiteoctober/breadcrumbs-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/lunetics/locale-bundle)
  lunetics/locale-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/notification-bundle)
  sonata-project/notification-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/gregwar/image-bundle)
  gregwar/image-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sg/datatablesbundle)
  sg/datatablesbundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sensio/buzz-bundle)
  sensio/buzz-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/ornicar/gravatar-bundle)
  ornicar/gravatar-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/fr3d/ldap-bundle)
  fr3d/ldap-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/liuggio/statsd-client-bundle)
  liuggio/statsd-client-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/pugx/multi-user-bundle)
  pugx/multi-user-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sylius/addressing-bundle)
  sylius/addressing-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/doctrine-mongodb-admin-bundle)
  sonata-project/doctrine-mongodb-admin-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/doctrine-phpcr-admin-bundle)
  sonata-project/doctrine-phpcr-admin-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jmikola/auto-login-bundle)
  jmikola/auto-login-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/eko/feedbundle)
  eko/feedbundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/craue/twigextensions-bundle)
  craue/twigextensions-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/dms/dms-filter-bundle)
  dms/dms-filter-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/craue/geo-bundle)
  craue/geo-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/jquery-bundle)
  sonata-project/jquery-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/menu-bundle)
  symfony-cmf/menu-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/core-bundle)
  symfony-cmf/core-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/block-bundle)
  symfony-cmf/block-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/tedivm/stash-bundle)
  tedivm/stash-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/tree-browser-bundle)
  symfony-cmf/tree-browser-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sylius/inventory-bundle)
  sylius/inventory-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/pulse00/ffmpeg-bundle)
  pulse00/ffmpeg-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/sylius/core-bundle)
  sylius/core-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/noiselabs/nusoap-bundle)
  noiselabs/nusoap-bundle

### Bundles with 100/month popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jbtronics/settings-bundle)
  jbtronics/settings-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/propel/propel-bundle)
  propel/propel-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/makinacorpus/db-tools-bundle)
  makinacorpus/db-tools-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/avalanche123/imagine-bundle)
  avalanche123/imagine-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/friendsofsymfony/facebook-bundle)
  friendsofsymfony/facebook-bundle
* 
  raulfraile/ladybug-bundle 1310
  ![Packagist Downloads](https://img.shields.io/packagist/dm/raulfraile/ladybug-bundle)
* 
  zenstruck/redirect-bundle 1554
  ![Packagist Downloads](https://img.shields.io/packagist/dm/zenstruck/redirect-bundle)
* 
  igorw/file-serve-bundle 1587
  ![Packagist Downloads](https://img.shields.io/packagist/dm/igorw/file-serve-bundle)
* 
  sonata-project/news-bundle 1622
  ![Packagist Downloads](https://img.shields.io/packagist/dm/sonata-project/news-bundle)
* 
  jns/xhprof-bundle 1694
  ![Packagist Downloads](https://img.shields.io/packagist/dm/jns/xhprof-bundle)
* 
  sylius/cart-bundle 1704
  ![Packagist Downloads](https://img.shields.io/packagist/dm/sylius/cart-bundle)
* 
  bcc/cron-manager-bundle 1740
  ![Packagist Downloads](https://img.shields.io/packagist/dm/bcc/cron-manager-bundle)
* 
  jms/debugging-bundle 1756
  ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/debugging-bundle)
* 
  sp/bower-bundle 1762
  ![Packagist Downloads](https://img.shields.io/packagist/dm/sp/bower-bundle)
* 
  noiselabs/smarty-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/noiselabs/smarty-bundle)
* 
  bmatzner/lodash-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/bmatzner/lodash-bundle)
* 
  damianociarla/rating-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/damianociarla/rating-bundle)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/imag/ldap-bundle)
  imag/ldap-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/rgsystemes/olark-bundle)
  rgsystemes/olark-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/coresphere/console-bundle)
  coresphere/console-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/kitpages/data-grid-bundle)
  kitpages/data-grid-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/simple-cms-bundle)
  symfony-cmf/simple-cms-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/egulias/listeners-debug-command-bundle)
  egulias/listeners-debug-command-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/content-bundle)
  symfony-cmf/content-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/ismaambrosi/generator-bundle)
  ismaambrosi/generator-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/vmelnik/doctrine-encrypt-bundle)
  vmelnik/doctrine-encrypt-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jfsimon/gmap-bundle)
  jfsimon/gmap-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/avanzu/admin-theme-bundle)
  avanzu/admin-theme-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/fkr/cssurlrewrite-bundle)
  fkr/cssurlrewrite-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/helios-ag/fm-bbcode-bundle)
  helios-ag/fm-bbcode-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/yethee/enum-bundle)
  yethee/enum-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/bobthecow/mustache-bundle)
  bobthecow/mustache-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jdare/clank-bundle)
  jdare/clank-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/kayue/kayue-wordpress-bundle)
  kayue/kayue-wordpress-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/cg/kint-bundle)
  cg/kint-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/create-bundle)
  symfony-cmf/create-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/endroid/qrcode-bundle)
  endroid/qrcode-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/forkcms/forkcms)
  forkcms/forkcms
* 
  fkr/simplepie-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/fkr/simplepie-bundle)
* 
  oh/google-map-form-type-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/oh/google-map-form-type-bundle)
* 
  iphp/filestore-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/iphp/filestore-bundle)
* 
  vresh/twilio-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/vresh/twilio-bundle)
* 
  varnish/varnish-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/varnish/varnish-bundle)
* 
  asapo/tidy-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/asapo/tidy-bundle)

### Bundles with 10/month popularity
* shtumi/useful-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/shtumi/useful-bundle)
* neo4j/neo4j-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/neo4j/neo4j-bundle)

### Bundles with 1/month popularity
* eprofos/user-agent-analyzer 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/eprofos/user-agent-analyzer )
* ![Packagist Downloads](https://img.shields.io/packagist/dm/cedriclombardot/admingenerator-generator-bundle)
  cedriclombardot/
* 
  zerkalica/millwright-menu-bundle 1250
  ![Packagist Downloads](https://img.shields.io/packagist/dm/zerkalica/millwright-menu-bundle)
* 
  yepsua/smartwig-bundle 1283
  ![Packagist Downloads](https://img.shields.io/packagist/dm/yepsua/smartwig-bundle)
* 
  mopa/bootstrap-sandbox-bundle 1356
  ![Packagist Downloads](https://img.shields.io/packagist/dm/mopa/bootstrap-sandbox-bundle)
* 
  ghua/ext-direct-bundle 1449
  ![Packagist Downloads](https://img.shields.io/packagist/dm/ghua/ext-direct-bundle)
* 
  vbardales/multiple-app-kernel-bundle 1471
  ![Packagist Downloads](https://img.shields.io/packagist/dm/vbardales/multiple-app-kernel-bundle)
* 
  jdelaune/oauth2-client-bundle 1530
  ![Packagist Downloads](https://img.shields.io/packagist/dm/jdelaune/oauth2-client-bundle)

### Bundles with 0/month popularity
* stephpy/timelinebundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/stephpy/timelinebundle)
* becklyn/rad-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/becklyn/rad-bundle)
* punkave/symfony2-file-uploader-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/punkave/symfony2-file-uploader-bundle)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/makinacorpus/query-builder-bundle)
  makinacorpus/query-builder-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony2admingenerator/generator-bundle)
  symfony2admingenerator/generator-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jbi/browscap-bundle)
  jbi/browscap-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/endroid/tile-bundle)
  endroid/tile-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/novaway/filemanagementbundle)
  novaway/filemanagementbundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/ajgl/csv-bundle)
  ajgl/csv-bundle
* 
  imag/files-bundle 1215
  ![Packagist Downloads](https://img.shields.io/packagist/dm/imag/files-bundle)
* ![Packagist Downloads](https://img.shields.io/packagist/dm/rodgermd/compass-bundle)
  rodgermd/compass-bundle
* 
  ddeboer/salesforce-client-bundle 1218
  ![Packagist Downloads](https://img.shields.io/packagist/dm/ddeboer/salesforce-client-bundle)
* 
  leaseweb/remote-template-bundle 1227
  ![Packagist Downloads](https://img.shields.io/packagist/dm/leaseweb/remote-template-bundle)
* 
  presta/cms-core-bundle 1263
  ![Packagist Downloads](https://img.shields.io/packagist/dm/presta/cms-core-bundle)
* 
  knplabs/rad-bundle 1297
  ![Packagist Downloads](https://img.shields.io/packagist/dm/knplabs/rad-bundle)
* 
  berriart/sitemap-bundle 1307
  ![Packagist Downloads](https://img.shields.io/packagist/dm/berriart/sitemap-bundle)
* 
  js/mysqlnd-bundle 1320
  ![Packagist Downloads](https://img.shields.io/packagist/dm/js/mysqlnd-bundle)
* 
  kunstmaan/languagechooser-bundle 1357
  ![Packagist Downloads](https://img.shields.io/packagist/dm/kunstmaan/languagechooser-bundle)
* 
  herzult/forum-bundle 1363
  ![Packagist Downloads](https://img.shields.io/packagist/dm/herzult/forum-bundle)
* 
  presta/cms-media-bundle 1365
  ![Packagist Downloads](https://img.shields.io/packagist/dm/presta/cms-media-bundle)
* 
  nekland/feed-bundle 1402
  ![Packagist Downloads](https://img.shields.io/packagist/dm/nekland/feed-bundle)
* 
  happyr/mailer-bundle 1446
  ![Packagist Downloads](https://img.shields.io/packagist/dm/happyr/mailer-bundle)
* 
  claroline/forum-bundle 1479
  ![Packagist Downloads](https://img.shields.io/packagist/dm/claroline/forum-bundle)
* 
  rezzza/sepa-bundle 1496
  ![Packagist Downloads](https://img.shields.io/packagist/dm/rezzza/sepa-bundle)
* 
  xi/ajax-bundle 1507
  ![Packagist Downloads](https://img.shields.io/packagist/dm/xi/ajax-bundle)
* 
  rezzza/flickr-bundle 1511
  ![Packagist Downloads](https://img.shields.io/packagist/dm/rezzza/flickr-bundle)
* 
  endroid/prediction-io 1520
  ![Packagist Downloads](https://img.shields.io/packagist/dm/endroid/prediction-io)
* 
  rc2c/form-twitter-bundle 1556
  ![Packagist Downloads](https://img.shields.io/packagist/dm/rc2c/form-twitter-bundle)
* 
  claroline/core-bundle 1643
  ![Packagist Downloads](https://img.shields.io/packagist/dm/claroline/core-bundle)
* 
  netteam/datatable-bundle 1644
  ![Packagist Downloads](https://img.shields.io/packagist/dm/netteam/datatable-bundle)
* 
  kitpages/pdf-bundle 1681
  ![Packagist Downloads](https://img.shields.io/packagist/dm/kitpages/pdf-bundle)
* 
  jms/command-bundle 1686
  ![Packagist Downloads](https://img.shields.io/packagist/dm/jms/command-bundle)
* 
  xi/breadcrumbs-bundle 1692
  ![Packagist Downloads](https://img.shields.io/packagist/dm/xi/breadcrumbs-bundle)
* 
  salva/pdfjs-bundle 1699
  ![Packagist Downloads](https://img.shields.io/packagist/dm/salva/pdfjs-bundle)
* 
  xi/dialog-bundle 1708
  ![Packagist Downloads](https://img.shields.io/packagist/dm/xi/dialog-bundle)
* 
  jmikola/js-assets-helper-bundle 1718
  ![Packagist Downloads](https://img.shields.io/packagist/dm/jmikola/js-assets-helper-bundle)
* 
  kphoen/sms-sender-bundle 1738
  ![Packagist Downloads](https://img.shields.io/packagist/dm/kphoen/sms-sender-bundle)
* 
  innova/angular-ui-pageslide-bundle 1742
  ![Packagist Downloads](https://img.shields.io/packagist/dm/innova/angular-ui-pageslide-bundle)
* 
  knplabs/knp-zend-cache-bundle 1761
  ![Packagist Downloads](https://img.shields.io/packagist/dm/knplabs/knp-zend-cache-bundle)
* 
  symfonycontrib/filefield-bundle 1773
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfonycontrib/filefield-bundle)
* 
  nedwave/mandrill-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/nedwave/mandrill-bundle)
* 
  pecserke/yaml-fixtures-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/pecserke/yaml-fixtures-bundle)
* 
  webit/bcmath-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/webit/bcmath-bundle)
* 
  friendsofsymfony/twitter-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/friendsofsymfony/twitter-bundle)
* 
  doctrine/mongodb-odm-softdelete-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/mongodb-odm-softdelete-bundle)
* 
  elcodi/cart-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/elcodi/cart-bundle)
* 
  symfonycontrib/imagefield-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfonycontrib/imagefield-bundle)
* 
  snowcap/mailjet-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/snowcap/mailjet-bundle)
* 
  doctrine/couchdb-odm-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/couchdb-odm-bundle)

### Bundles with unclassified popularity
* ![Packagist Downloads](https://img.shields.io/packagist/dm/opensky/runtime-config-bundle)
  opensky/runtime-config-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/khepin/yaml-fixtures-bundle)
  khepin/yaml-fixtures-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/medicorenl/jira-api-bundle)
  medicorenl/jira-api-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/dizda/cloud-backup-bundle)
  dizda/cloud-backup-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/evp/gsms-bundle)
  evp/gsms-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/netdudes/jstreebundle)
  netdudes/jstreebundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jmikola/wildcard-event-dispatcher-bundle)
  jmikola/wildcard-event-dispatcher-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/uam/postmark-swiftmailer-bundle)
  uam/postmark-swiftmailer-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/rollerworks/navigation-bundle)
  rollerworks/navigation-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/armetiz/facebook-bundle)
  armetiz/facebook-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/zeroem/curl-bundle)
  zeroem/curl-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/floriansemm/solr-bundle)
  floriansemm/solr-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/apy/jsfv-bundle)
  apy/jsfv-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/uam/aws-ecs-bundle)
  uam/aws-ecs-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/routing-extra-bundle)
  symfony-cmf/routing-extra-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/henrikbjorn/stampie-bundle)
  henrikbjorn/stampie-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/uam/amazon-search-bundle)
  uam/amazon-search-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/appventus/ajax-bundle)
  appventus/ajax-bundle
* ![Packagist Downloads](https://img.shields.io/packagist/dm/scribe/stripe-bundle)
  scribe/stripe-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/snowcap/bootstrap-bundle)
  snowcap/bootstrap-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/widop/twig-extensions-bundle)
  widop/twig-extensions-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/benji07/akismet-bundle)
  benji07/akismet-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/tarjei/memcachebundle)
  tarjei/memcachebundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/ornicar/akismet-bundle)
  ornicar/akismet-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/jlm/aws-bundle)
  jlm/aws-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/ekino/wordpress-bundle)
  ekino/wordpress-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/webignition/doctrine-migrations-bundle)
  webignition/doctrine-migrations-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/snowcap/admin-bundle)
  snowcap/admin-bundle
*
  ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony-cmf/search-bundle)
  symfony-cmf/search-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/yuriteixeira/workflow-bundle)
  yuriteixeira/workflow-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/padam87/attribute-bundle)
  padam87/attribute-bundle
* 
  ![Packagist Downloads](https://img.shields.io/packagist/dm/winzou/console-bundle)
  winzou/console-bundle
* 
  mattsches/version-eye-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/mattsches/version-eye-bundle)
* 
  kitpages/file-system-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/kitpages/file-system-bundle)
* 
  brazilianfriendsofsymfony/twig-extensions-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/brazilianfriendsofsymfony/twig-extensions-bundle)
* 
  nyrodev/utility-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/nyrodev/utility-bundle)
* 
  igraal/twig-assetic-filter-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/igraal/twig-assetic-filter-bundle)
* 
  snowcap/i18n-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/snowcap/i18n-bundle)
* 
  pomm/pomm-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/pomm/pomm-bundle)
* 
  snowcap/im-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/snowcap/im-bundle)
* 
  search/sphinxsearch-bundle 3568 ---
  ![Packagist Downloads](https://img.shields.io/packagist/dm/search/sphinxsearch-bundle)
* ...
* 
  twitter/bootstrap-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/twitter/bootstrap-bundle)
* 
  ideup/simple-paginator-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/ideup/simple-paginator-bundle)
* 
  innocead/captcha-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/innocead/captcha-bundle)
* 
  bmatzner/foundation-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/bmatzner/foundation-bundle)
* 
  goc/pagination-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/goc/pagination-bundle)
* 
  rollerworks/search-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/rollerworks/search-bundle)

### Bundles with popularity not found
* 
  excelwebzone/omlex-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/excelwebzone/omlex-bundle)
* 
  excelwebzone/search-bundle
  ![Packagist Downloads](https://img.shields.io/packagist/dm/excelwebzone/search-bundle)

### Searching for bundles
* https://packagist.org/?type=symfony-bundle
* https://packagist.org/packages/symfony/framework-bundle/dependents?order_by=downloads

#### Offline
* https://phppackages.org/s/symfony
* https://phppackages.org/s/bundle

## Projects
* [symfony/framework-standard-edition
  ](https://packagist.org/packages/symfony/framework-standard-edition)
  This package is abandoned and no longer maintained. No replacement package was suggested.

# A Week of Symfony
## 2019
* [#663 (9-15 September 2019)](https://symfony.com/blog/a-week-of-symfony-663-9-15-september-2019)
  Symfony Notifier presentation
